import { create } from 'zustand'

type Store = {
  hash: string
  idHotel: number;
  inc: (hash:string) => void
  setIdHotel: (id_hotel:number) => void
}

export const useDataStore = create<Store>()((set) => ({
  hash: "",
  idHotel: -1,
  inc: (hash) => set({ hash }),
  setIdHotel: (idHotel) => set({ idHotel })
}))

/*
Creamos un estado global para actualizar la interfaz
con los datos cada vez que se interactue con las
peticiones en la API.
*/