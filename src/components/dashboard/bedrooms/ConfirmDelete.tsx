import {
  AlertDialog,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogTrigger,
} from "@/components/ui/alert-dialog"
import { Button } from "@/components/ui/button"
import { useDataStore } from "@/store/useDataStore";
import { v4 } from "uuid";

interface ConfirmDeleteProps {
  id: string;
}

const ConfirmDelete = (props:ConfirmDeleteProps) => {

  const { inc, setIdHotel } = useDataStore()
  const deleteHab = async () => {
    const response = await fetch(`http://146.190.32.176/diplomado/api/rooms/${props.id}`,{
      method: 'DELETE'
    })

    if(response){
      alert("Habitación eliminada");
      inc(v4());
      const data = await response.json();
      console.log(data);
      setIdHotel(data.data.hotel_id)
    }
  }
  return (
    <AlertDialog>
  <AlertDialogTrigger><Button variant="destructive" >ELIMINAR</Button></AlertDialogTrigger>
  <AlertDialogContent>
    <AlertDialogHeader>
      <AlertDialogTitle className="text-red-500">Eliminar la habitación H-01 del hotel GHL?</AlertDialogTitle>
      <AlertDialogDescription>
        Esta acción no se puede deshacer, y la habitación será borrada definitivamente.
      </AlertDialogDescription>
    </AlertDialogHeader>
    <AlertDialogFooter>
      <AlertDialogCancel>Cancelar</AlertDialogCancel>
      <AlertDialogAction 
        className="bg-red-500 text-white"
        onClick={deleteHab}
      >
        Continuar
      </AlertDialogAction>
    </AlertDialogFooter>
  </AlertDialogContent>
</AlertDialog>

  )
}

export default ConfirmDelete