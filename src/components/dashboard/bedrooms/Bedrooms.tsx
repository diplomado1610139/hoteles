"use client"
import Aside from './Aside'
import { TabsContent } from '@/components/ui/tabs'
import ListBedrooms from './ListBedrooms'
import {
  Sheet,
  SheetContent,
  SheetHeader,
  SheetTitle,
  SheetTrigger,
} from "@/components/ui/sheet"
import { Input } from '@/components/ui/input'
import { Button } from "@/components/ui/button"
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form"
import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import * as z from "zod"
import { toast } from "@/components/ui/use-toast"
import { BedroomSchema } from "@/_schemas/hotel.schema"
import { useDataStore } from '@/store/useDataStore'
import { useEffect, useState } from 'react'
import { v4 } from 'uuid'
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from '@/components/ui/select'

interface BedroomsProps{
  id:number;
}

const Bedrooms = (props:BedroomsProps) => {

  const { inc } = useDataStore();
  const [tiposHabitaciones, setTiposHabitaciones] = useState<Array<any>>([]);

  const obtenerTiposHabitaciones = async () => {
    let response = null
    const data = await fetch("http://146.190.32.176/diplomado/api/room-types", {
      method: "GET"
    })
    if (data) {
      const response = await data.json();
      return response;
    }
    return response;
  }

  useEffect(() => {
    (async () => {
      const habs = await obtenerTiposHabitaciones()
      if (habs) {
        console.log(habs);
        setTiposHabitaciones(habs);
      }
    })();
  }, [])

  async function onSubmit(data: z.infer<typeof BedroomSchema>) {
    try {
      console.log(JSON.stringify(data));
      const response = await fetch("http://146.190.32.176/diplomado/api/rooms", {
        method: "POST",
        headers: {
          "Content-Type": "application/json", // Use "Content-Type" for the request body media type
        },
        body: JSON.stringify({
          hotel_id: props.id,
          ...data
        })
      })
      const content = await response.json();
      if(content.errors){
        alert(content.message);
      } else {
        alert("habitación guardada!");
        inc(v4());
      }
      inc(v4());
        
      
    } catch (error) {
      alert("Ha ocurrido un error interno" + error);
    }
  }

  const form = useForm<z.infer<typeof BedroomSchema>>({
    resolver: zodResolver(BedroomSchema),
  })

  return (
    <TabsContent value="bedrooms">
      <div className='grid grid-cols-5 gap-2'>
        <div className='col-span-1' >
          <Aside />
        </div>
        <div className='col-span-4'>
          <ListBedrooms />
          <Sheet>
            <SheetTrigger><Button variant="secondary">CREAR</Button></SheetTrigger>
            <SheetContent>
              <SheetHeader>
                <SheetTitle>Crear habitación</SheetTitle>
                <Form {...form}>
                  <form onSubmit={form.handleSubmit(onSubmit)} className="w-full space-y-6">
                  <FormField
                      control={form.control}
                      name="quantity"
                      render={({ field }) => (
                        <FormItem>
                          <FormLabel>Cantidad</FormLabel>
                          <FormControl>
                            <Input type='number' placeholder="1" {...field} />
                          </FormControl>
                          <FormMessage />
                        </FormItem>
                      )}
                    />
                    <FormField
                      control={form.control}
                      name="room_type_id"
                      render={({ field }) => (
                        <FormItem>
                          <FormLabel>Tipo</FormLabel>
                          <Select onValueChange={field.onChange} defaultValue={field.value}>
                            <FormControl>
                              <SelectTrigger>
                                <SelectValue placeholder="Seleccione tipo de habitación" />
                              </SelectTrigger>
                            </FormControl>
                            <SelectContent>
                              {
                                tiposHabitaciones && tiposHabitaciones.map(hab => (
                                  <SelectItem key={hab.id} value={String(hab.id)}>{hab.name}</SelectItem>
                                ))
                              }
                            </SelectContent>
                          </Select>
                        </FormItem>
                      )}
                    />
                    
                    <FormField
                      control={form.control}
                      name="accommodation_id"
                      render={({ field }) => (
                        <FormItem>
                          <FormLabel>Tipo de acomodación</FormLabel>
                          <Select onValueChange={field.onChange} defaultValue={field.value}>
                            <FormControl>
                              <SelectTrigger>
                                <SelectValue placeholder="Seleccione tipo de acomodación" />
                              </SelectTrigger>
                            </FormControl>
                            <SelectContent>
                              <SelectItem value="1">Sencilla</SelectItem>
                              <SelectItem value="2">Doble</SelectItem>
                              <SelectItem value="3">Triple</SelectItem>
                            </SelectContent>
                          </Select>
                        </FormItem>
                      )}
                    />
                    <Button type="submit">Guardar</Button>
                  </form>
                </Form>
              </SheetHeader>
            </SheetContent>
          </Sheet>
        </div>
      </div>
    </TabsContent>
  )
}

export default Bedrooms