import {
  Table,
  TableBody,
  TableCaption,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table"
import EditBedroom from "./EditBedroom"
import ConfirmDelete from "./ConfirmDelete"
import { useEffect, useState } from "react";
import { useDataStore } from "@/store/useDataStore";

interface EditBedroomProps {
  id:string;
  
}

const ListBedrooms = () => {
  const [habitaciones, setHabitaciones] = useState<Array<any>>([]);
  const { idHotel } = useDataStore();

  const obtenerHabitaciones = async () => {
    let response = null
    const data = await fetch(`http://146.190.32.176/diplomado/api/rooms/${idHotel}`, {
      method: "GET"
    })
    if (data) {
      response = await data.json();
      return response.data;
    }
    return response;
  }

  useEffect(() => {
    (async () => {
      const habs = await obtenerHabitaciones()
      if (habs) {
        setHabitaciones(habs);
      }
    })();
  }, [idHotel])
  return (
    <Table className="bg-gray-900">
      {
        habitaciones.length > 0 ? <TableCaption>Habitaciones de {habitaciones[0]?.hotel.name}.</TableCaption> : null
      }
      <TableHeader>
        <TableRow>
          <TableHead className="w-[100px]">Cantidad</TableHead>
          <TableHead>Tipo</TableHead>
          <TableHead>Acomodación</TableHead>
          <TableHead className="text-right">Opciones</TableHead>
        </TableRow>
      </TableHeader>
      <TableBody>
        {
          habitaciones && habitaciones.map(hab => (
            <TableRow key={hab.id}>
              <TableCell className="font-medium">{hab.quantity}</TableCell>
              <TableCell>{hab.type.name}</TableCell>
              <TableCell>{hab.accommodation.name}</TableCell>
              <TableCell className="justify-end flex gap-2">
                <EditBedroom
                  id={hab.hotel_id}
                  id_bedroom={hab.id}
                  accommodation_id={hab.accommodation.id}
                  quantity={hab.quantity}
                  room_type_id={hab.room_type_id}
                />
                <ConfirmDelete
                  id={hab.id}
                />
              </TableCell>
            </TableRow>
          ))
        }
      </TableBody>
    </Table>

  )
}

export default ListBedrooms