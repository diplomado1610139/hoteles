import {
  Command,
  CommandDialog,
  CommandEmpty,
  CommandGroup,
  CommandInput,
  CommandItem,
  CommandList,
  CommandSeparator,
  CommandShortcut,
} from "@/components/ui/command"
import { useDataStore } from "@/store/useDataStore";
import { useEffect, useState } from "react";

const Aside = () => {

  const [hoteles, setHoteles] = useState<Array<any>>([]);
  const { setIdHotel } = useDataStore();

  const obtenerHoteles = async () => {
    let response = null
    const data = await fetch("http://146.190.32.176/diplomado/api/hotels", {
      method: "GET"
    })
    if (data) {
      response = await data.json();
      return response.data;
    }
    return response;
  }

  useEffect(() => {
    (async () => {
      const hotels = await obtenerHoteles()
      if (hotels) {
        console.log(hotels);
        setHoteles(hotels);
      }
    })();
  }, [])

  return (
    <Command className="bg-gray-900">
      <CommandInput placeholder="Busca un hotel..." />
      <CommandList>
        <CommandEmpty>No se encontro ningun hotel.</CommandEmpty>
        <CommandGroup heading="Hoteles">
          {
            hoteles && hoteles.map(hotel => (
              <CommandItem key={hotel.id}>
                <div className="w-full" key={hotel.id} onClick={() => setIdHotel(hotel.id)} >
                  {hotel.name} - {hotel.nit}
                </div>
              </CommandItem>
            ))
          }
        </CommandGroup>
      </CommandList>
    </Command>
  )
}

export default Aside