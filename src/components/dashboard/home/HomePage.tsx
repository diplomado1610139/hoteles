//Estas son las importaciones para usar Chart.js
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
import { faker } from '@faker-js/faker';
import { TabsContent } from '@/components/ui/tabs';

import * as React from "react"

import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select"
import { useState } from 'react';

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

export const options = {
  responsive: true,
  plugins: {
    legend: {
      position: 'top' as const,
    },
    title: {
      display: true,
      text: 'Cantidad de habitaciones por tipos',
    },
  },
};

export const options2 = {
  responsive: true,
  plugins: {
    legend: {
      position: 'top' as const,
    },
    title: {
      display: true,
      text: 'Cantidad de acomodaciones por tipo',
    },
  },
};

const labels = ['Estándar', 'Junior', 'Suite'];
const labels2 = ['Sencilla', 'Doble', 'Triple', 'Cuádruple'];

export const data = {
  labels,
  datasets: [
    {
      label: 'No. de habitaciones',
      data: labels.map(() => faker.datatype.number({ min: 0, max: 1000 })),
      backgroundColor: 'rgba(255, 99, 132, 0.5)',
    }
  ],
};

export const data2 = {
  labels: labels2,
  datasets: [
    {
      label: 'No. de acomodaciones',
      data: labels2.map(() => faker.datatype.number({ min: 0, max: 1000 })),
      backgroundColor: 'rgba(26, 245, 237, 0.5)',
    }
  ],
};

const HomePage = () => {
  const [selected, setSelected] = useState<string>("")
  return (
    <>
      <TabsContent value="home">
        <Select onValueChange={setSelected}>
          <SelectTrigger className="w-[180px] bg-gray-800 px-2 mt-1 py-1 rounded-md">
            <SelectValue placeholder="Seleccionar" />
          </SelectTrigger>
          <SelectContent>
            <SelectGroup>
              <SelectItem value="Hab. por tipos">Hab. por tipos</SelectItem>
              <SelectItem value="Acom. por tipos">Acom. por tipos</SelectItem>
            </SelectGroup>
          </SelectContent>
        </Select>

        {
          selected == "Hab. por tipos" ?
            <div className='md:mx-32'>
              <Bar options={options} data={data} />
            </div>
            :
            <div className='md:mx-32'>
              <Bar options={options2} data={data2} />
            </div>
        }
      </TabsContent>
    </>
  )
}

export default HomePage