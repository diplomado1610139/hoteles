"use client"
import { TabsContent } from '../../ui/tabs'
import ListHotels from "./ListHotels"
import {
  Sheet,
  SheetContent,
  SheetDescription,
  SheetHeader,
  SheetTitle,
  SheetTrigger,
} from "@/components/ui/sheet"
import { Input } from '@/components/ui/input'
import { Button } from "@/components/ui/button"
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form"
import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import * as z from "zod"
import { toast } from "@/components/ui/use-toast"
import { FormSchema } from "@/_schemas/hotel.schema"
import { useRouter } from 'next/navigation'

import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select"
import { useEffect, useState } from 'react'
import { useDataStore } from "@/store/useDataStore"
import { v4 } from 'uuid';


const Hotels = () => {
  const { inc } = useDataStore();
  const [ ciudades, setCiudades ] = useState<Array<any>>([]);

  const obtenerCiudades = async () => {
    let response = null
    const data = await fetch("http://146.190.32.176/diplomado/api/cities", {
      method: "GET"
    })
    if(data){
      const response = await data.json();
      return response;
    }
    return response;
  }

  useEffect(() => {
    (async() => {
      const cities = await obtenerCiudades()
      if(cities){
        console.log(cities);
        setCiudades(cities);
      }
    })();
  }, [])

  function onSubmit(data: z.infer<typeof FormSchema>) {
    try {
      console.log(JSON.stringify(data));
      fetch("http://146.190.32.176/diplomado/api/hotels", {
        method: "POST",
        headers: {
          "Content-Type": "application/json", // Use "Content-Type" for the request body media type
        },
        body: JSON.stringify(data)
      }).then(() => {
        alert("Hotel guardado!");
        inc(v4());

      });
    } catch (error) {
      alert("Ha ocurrido un error interno" + error);
    }
  }  

  const form = useForm<z.infer<typeof FormSchema>>({
    resolver: zodResolver(FormSchema),
  })

  return (
    <TabsContent value="hotels">
      <ListHotels />

      {/* Create a hotel */}
      <Sheet>
        <SheetTrigger
          className="bg-slate-100 text-gray-700 rounded-full px-4 py-2 text-2xl float-right"
        >
          +
        </SheetTrigger>
        <SheetContent>
          <SheetHeader>
            <SheetTitle>Registrar hotel</SheetTitle>
            <Form {...form}>
              <form onSubmit={form.handleSubmit(onSubmit)} className="w-full space-y-6">
                <FormField
                  control={form.control}
                  name="city_id"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Ciudad</FormLabel>
                      <Select onValueChange={field.onChange} defaultValue={field.value}>
                        <FormControl>
                          <SelectTrigger>
                            <SelectValue placeholder="Seleccione la ciudad" />
                          </SelectTrigger>
                        </FormControl>
                        <SelectContent>
                          {
                            ciudades && ciudades.map(ciudad => (
                              <SelectItem key={ciudad.id} value={String(ciudad.id)}>{ciudad.name}</SelectItem>
                            ))
                          }
                          </SelectContent>
                      </Select>
                    </FormItem>
                  )}
                />


                <FormField
                  control={form.control}
                  name="name"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Nombre</FormLabel>
                      <FormControl>
                        <Input placeholder="Hotel GHL" {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name="nit"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Nit</FormLabel>
                      <FormControl>
                        <Input placeholder="1045722909-2" {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name="address"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Dirección</FormLabel>
                      <FormControl>
                        <Input placeholder="Calle 26 No. 15w - 15" {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name="num_rooms"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Num. max de habitaciones</FormLabel>
                      <FormControl>
                        <Input placeholder='42' type='number' {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <Button type="submit">Registrar</Button>
              </form>
            </Form>
          </SheetHeader>
        </SheetContent>
      </Sheet>
    </TabsContent>
  )
}

export default Hotels;