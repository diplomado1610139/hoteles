import { Button } from "@/components/ui/button"
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog"

interface LookDataProps {
  name:string;
  nit:string;
  city:string;
  address:string;
  no_bedrooms:number;
}

export const LookData = (props:LookDataProps) => {
  return (
    <Dialog>
      <DialogTrigger><Button className="underline" variant="link">VER DETALLES</Button></DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>{props.name.toUpperCase()}</DialogTitle>
          <DialogDescription>
            <ul className="py-4 flex flex-col gap-1 border-t-2 border-teal-900 ">
              <li><strong>NIT:</strong> {props.nit}</li>
              <li><strong>CIUDAD:</strong>{props.city}</li>
              <li><strong>DIRECCIÓN:</strong>{props.address}</li>
              <li><strong>MAX DE HABITACIONES:</strong>{props.no_bedrooms}</li>
            </ul>
          </DialogDescription>
        </DialogHeader>
      </DialogContent>
    </Dialog>

  )
}
