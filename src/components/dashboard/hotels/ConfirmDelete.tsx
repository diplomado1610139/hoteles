import {
  AlertDialog,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogTrigger,
} from "@/components/ui/alert-dialog"
import { Button } from "@/components/ui/button"
import { useDataStore } from "@/store/useDataStore";
import { v4 } from "uuid";

interface ConfirmDeleteProps {
  id: string;
}

const ConfirmDelete = (props:ConfirmDeleteProps) => {
  const { inc } = useDataStore()
  const deleteHotel = async () => {
    const response = await fetch(`http://146.190.32.176/diplomado/api/hotels/${props.id}`,{
      method: 'DELETE'
    })

    if(response){
      alert("Hotel eliminado");
      inc(v4()); 
    }
  }

  return (
    <AlertDialog>
      <AlertDialogTrigger><Button variant="destructive">ELIMINAR</Button></AlertDialogTrigger>
      <AlertDialogContent>
        <AlertDialogHeader>
          <AlertDialogTitle className="text-red-500">Eliminar a hotel GHL?</AlertDialogTitle>
          <AlertDialogDescription>
            Esta acción no se puede deshacer, y el hotel será borrado definitivamente.
          </AlertDialogDescription>
        </AlertDialogHeader>
        <AlertDialogFooter>
          <AlertDialogCancel>Cancelar</AlertDialogCancel>
          <AlertDialogAction 
            className="bg-red-500 text-white"
            onClick={deleteHotel}
          >
            Continuar
          </AlertDialogAction>
        </AlertDialogFooter>
      </AlertDialogContent>
    </AlertDialog>
  )
}

export default ConfirmDelete