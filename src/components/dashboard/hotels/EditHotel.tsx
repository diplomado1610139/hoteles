"use client"
import {
  Sheet,
  SheetContent,
  SheetDescription,
  SheetHeader,
  SheetTitle,
  SheetTrigger,
} from "@/components/ui/sheet"
import { Input } from '@/components/ui/input'
import { Button } from "@/components/ui/button"
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form"
import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import * as z from "zod"
import { toast } from "@/components/ui/use-toast"
import { FormSchema } from "@/_schemas/hotel.schema"
import { useEffect, useState } from "react"
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from "@/components/ui/select"
import { useDataStore } from "@/store/useDataStore"
import { v4 } from "uuid"

interface EditHotelProps {
  id:string;
  city_id: string;
  name: string;
  nit: string;
  address: string;
  num_rooms: string;
}

const EditHotel = (props:EditHotelProps) => {
  const [ ciudades, setCiudades ] = useState<Array<any>>([]);
  const { inc } = useDataStore();

  const obtenerCiudades = async () => {
    let response = null
    const data = await fetch("http://146.190.32.176/diplomado/api/cities", {
      method: "GET"
    })
    if(data){
      response = await data.json();
      return response;
    }
    return response;
  }

  function onSubmit(data: z.infer<typeof FormSchema>) {
    try {
      console.log(JSON.stringify(data));
      fetch(`http://146.190.32.176/diplomado/api/hotels/${props.id}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json", // Use "Content-Type" for the request body media type
        },
        body: JSON.stringify(data)
      }).then(() => {
        alert("Hotel actualizado!");
        inc(v4());
      });
    } catch (error) {
      alert("Ha ocurrido un error interno" + error);
    }
  }  

  const form = useForm<z.infer<typeof FormSchema>>({
    resolver: zodResolver(FormSchema),
  })

  useEffect(() => {
    (async() => {
      const cities = await obtenerCiudades()
      if(cities){
        console.log(cities);
        setCiudades(cities);
      }
    })();
  }, [])
  
  return (
    <Sheet>
        <SheetTrigger><Button variant="secondary">EDITAR</Button></SheetTrigger>
        <SheetContent>
          <SheetHeader>
            <SheetTitle>Editar hotel</SheetTitle>
            <Form {...form}>
              <form onSubmit={form.handleSubmit(onSubmit)} className="w-full space-y-6">
              <FormField
                  control={form.control}
                  name="city_id"
                  defaultValue={String(props.city_id)}
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Ciudad</FormLabel>
                      <Select onValueChange={field.onChange} defaultValue={String(props.city_id)}>
                        <FormControl>
                          <SelectTrigger>
                            <SelectValue placeholder="Seleccione la ciudad" />
                          </SelectTrigger>
                        </FormControl>
                        <SelectContent>
                          {
                            ciudades && ciudades.map(ciudad => (
                              <SelectItem key={ciudad.id} value={String(ciudad.id)}>{ciudad.name}</SelectItem>
                            ))
                          }
                          </SelectContent>
                      </Select>
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name="name"
                  defaultValue={props.name}
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Nombre</FormLabel>
                      <FormControl>
                        <Input defaultValue={props.name} placeholder="Hotel GHL" {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name="nit"
                  defaultValue={props.nit}
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Nit</FormLabel>
                      <FormControl>
                        <Input defaultValue={props.nit} placeholder="1045722909-2" {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name="address"
                  defaultValue={props.address}
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Dirección</FormLabel>
                      <FormControl>
                        <Input defaultValue={props.address} placeholder="Calle 26 No. 15w - 15" {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name="num_rooms"
                  defaultValue={String(props.num_rooms)}
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Num. max de habitaciones</FormLabel>
                      <FormControl>
                        <Input defaultValue={String(props.num_rooms)} placeholder='42' {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <Button type="submit">Actualizar</Button>
              </form>
            </Form>
          </SheetHeader>
        </SheetContent>
      </Sheet>
  )
}

export default EditHotel