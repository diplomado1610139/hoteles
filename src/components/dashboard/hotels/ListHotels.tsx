import { Button } from "@/components/ui/button"
import {
  Table,
  TableBody,
  TableCaption,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table"
import { LookData } from "./LookData"
import EditHotel from "./EditHotel"
import ConfirmDelete from "./ConfirmDelete"
import { ChangeEvent, useEffect, useState } from "react"
import { useRouter } from "next/navigation"
import { useDataStore } from "@/store/useDataStore"


const ListHotels = () => {

  const router = useRouter();
  const { hash } = useDataStore()

  //http://146.190.32.176/diplomado/api/hotels
  const [hoteles, setHoteles] = useState<Array<any>>([]);
  const [hotelById, setHotelById] = useState<Array<any>>([]);

  const obtenerHoteles = async () => {
    let response = null
    const data = await fetch("http://146.190.32.176/diplomado/api/hotels", {
      method: "GET"
    })
    if (data) {
      response = await data.json();
      return response.data;
    }
    return response;
  }

  useEffect(() => {
    (async () => {
      const hotels = await obtenerHoteles()
      if (hotels) {
        console.log(hotels);
        setHoteles(hotels);
      }
    })();
  }, [hash])

  const handleSearch = async (e: ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    if(e.target.value){
      let response = null
      let array = [];
      const data:any = await fetch(`http://146.190.32.176/diplomado/api/hotels/${e.target.value}`, {
        method: "GET"
      })
      console.log(data);
      if (data) {
        response = await data.json();
        if(response.data){
          array.push(response.data);
          setHotelById(array);
        } else {
          setHotelById([]);
        }
      } else {
        setHotelById([]);
      }
    } else {
      setHotelById([]);
    } 
  }

  return (
    <>
      <div className='my-3 flex items-start'>
        <div className="flex items-center max-w-md bg-gray-800 rounded-lg " x-data="{ search: '' }">
          <div className="w-full">
            <input type="search" className="w-full px-4 py-1 text-gray-400 bg-gray-800 rounded-full focus:outline-none"
              placeholder="Ingrese ID" x-model="search"
              onChange={(e) => handleSearch(e)}
            />
          </div>
          <div>
            <button type="submit" className="flex items-center bg-teal-700 justify-center w-12 h-12 text-white rounded-r-lg">
              <svg className="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                  d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
              </svg>
            </button>
          </div>
        </div>
      </div>
      <Table>
        <TableCaption>Una lista de tus hoteles.</TableCaption>
        <TableHeader>
          <TableRow className="bg-teal-800 hover:bg-teal-700 text-white">
            <TableHead className="w-[120px]">ID</TableHead>
            <TableHead className="w-[300px]">Nit</TableHead>
            <TableHead>Nombre</TableHead>
            <TableHead className="text-right">Opciones</TableHead>
          </TableRow>
        </TableHeader>
        <TableBody className="bg-slate-900" >
          {
            hoteles && hotelById.length == 0 ? hoteles.map(hotel => (
              <TableRow>
                <TableCell className="font-medium">{hotel.id}</TableCell>
                <TableCell className="font-medium">{hotel.nit}</TableCell>
                <TableCell>{hotel.name}</TableCell>
                <TableCell className="justify-end flex gap-2">
                  {/* <Button className="underline" variant="link">VER DETALLES</Button> */}
                  <LookData
                    name={hotel.name}
                    nit={hotel.nit}
                    city={hotel.city.name}
                    address={hotel.address}
                    no_bedrooms={hotel.num_rooms}
                  />
                  <EditHotel 
                    address={hotel.address}
                    id={hotel.id}
                    city_id={hotel.city.id}
                    name={hotel.name}
                    nit={hotel.nit}
                    num_rooms={hotel.num_rooms}
                  />
                  <ConfirmDelete 
                    id={hotel.id}
                  />
                </TableCell>
              </TableRow>
            ))
            :
            hotelById.map(hotel => (
              <TableRow>
                <TableCell className="font-medium">{hotel.id}</TableCell>
                <TableCell className="font-medium">{hotel.nit}</TableCell>
                <TableCell>{hotel.name}</TableCell>
                <TableCell className="justify-end flex gap-2">
                  {/* <Button className="underline" variant="link">VER DETALLES</Button> */}
                  <LookData
                    name={hotel.name}
                    nit={hotel.nit}
                    city={hotel.city.name}
                    address={hotel.address}
                    no_bedrooms={hotel.num_rooms}
                  />
                  <EditHotel 
                    address={hotel.address}
                    id={hotel.id}
                    city_id={hotel.city_id}
                    name={hotel.name}
                    nit={hotel.nit}
                    num_rooms={hotel.num_rooms}
                  />
                  <ConfirmDelete 
                    id={hotel.id}
                  />
                </TableCell>
              </TableRow>
            ))
          }
        </TableBody>
      </Table>
    </>
  )
}

export default ListHotels