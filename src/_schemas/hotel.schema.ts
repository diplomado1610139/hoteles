import { z } from 'zod';

export const FormSchema = z.object({
  city_id: z.string().min(1, {
    message: "Debe ser un número",   //Deber ser así para el select
  }),
  name: z.string().min(2, {
    message: "Nombre deber tener al menos 2 carácteres.",
  }),
  nit: z.string().min(2, {
    message: "Nit debe tener al menos 2 carácteres.",
  }),
  address: z.string().min(2, {
    message: "Dirección debe tener al menos 2 carácteres.",
  }),
  num_rooms: z.string().min(1, {
    message: "Debe ser un número",   //Deber ser así para el select
  }),
})

export const BedroomSchema = z.object({
  quantity: z.string().min(1, {
    message: "Cantidad debe tener al menos 1 carácter.",
  }),
  room_type_id: z.string().min(1, {
    message: "Tipo debe tener al menos 1 carácter.",
  }),
  accommodation_id: z.string().min(1, {
    message: "Acomodación debe tener al menos 1 carácter.",
  })
})
