'use client'

import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs"
import Hotels from '@/components/dashboard/hotels/Hotels';
import Bedrooms from '@/components/dashboard/bedrooms/Bedrooms';
import HomePage from "@/components/dashboard/home/HomePage";
import { useEffect } from "react";
import { useDataStore } from "@/store/useDataStore";
import { useRouter } from "next/navigation";

const PageTabs = () => {
  const router = useRouter();
  const { hash, idHotel } = useDataStore()
  useEffect(() => {
    router.refresh();
  }, [hash])
  return (
      <Tabs defaultValue="home" className="w-full h-5/6">
        <div className='flex items-center pb-4 flex-row w-full justify-between border-b-[1px] border-dashed'>
          <TabsList className='border-2 border-dashed' >
            <TabsTrigger value="home">Home</TabsTrigger>
            <TabsTrigger value="hotels">Hoteles</TabsTrigger>
            <TabsTrigger value="bedrooms">Habitaciones</TabsTrigger>
          </TabsList>
          <h3 className="text-xl text-teal-600 tracking-tight">Asohoteles del Sinú</h3>
          <h2 className="text-3xl mr-8 font-bold tracking-tight">Dashboard</h2>
        </div>
        <HomePage/>
        <Hotels/>
        <Bedrooms
          id={idHotel}
        />
      </Tabs>
  );
}

export default PageTabs;